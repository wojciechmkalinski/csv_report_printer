### Description of program
        * This program is designed to print data from csv files to plain text. Firstly, he reads an user from user.csv file and then displays a files, which are dedicated for one of users.
	
### Functionalities
	* If you run program with parameter -c, it prints an output from csv files in newly created csv file instead of plain text. A file will be generated inside project. 
	* With parameter -top n where n is a number of items, you can display biggest size of files.


### Preparation
	Verify that you have installed a JDK
	a) cd /to/project/
	b) export JAVA_HOME=/usr/lib/jvm/name-of-jdk
	c) Launch these commands to make sure that sourcepath is correct and everything runs correctly
	$JAVA_HOME/bin/javac -sourcepath src src/com/egnyte/utils/auditreporter/Runner.java -d bin
	$JAVA_HOME/bin/java -cp bin com.wmk.utils.auditreporter.Runner resources/users.csv resources/files.csv 
	If you want to add some parameters, just add to the line.
### Tests
Test have been included with usage of jUnit.