package com.wmk.utils.auditreporter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Runner {

	
	//List of list of string - data from csv files located in resources folder.
	private List<List<String>> users;
	private List<List<String>> files;

	//Data from csv as a csv object.
	private List<Csv> filesObj = new ArrayList<Csv>();

	public List<Csv> getFilesObj() {
		return filesObj;
	}


	public static void main(String[] args) throws IOException {
		Runner r = new Runner();
		r.loadData(args[0], args[1]);
		r.run();
		r.filesAsObjects();
		if (args.length > 2) {
			if (args[2].equals("-c") || args[3].equals("-c")) {
				r.printToCsv();
			}
			if (args[2].equals("-n") || args[3].equals("-n")) {
				//Parameter n - how many sorted files by size you want to display
				int n = 3;
				r.topFiles(n);
			}
		}
	}

	private void run() {
		printHeader();
		for (List<String> userRow : users) {
			long userId = Long.parseLong(userRow.get(0));
			String userName = userRow.get(1);

			printUserHeader(userName);
			for (List<String> fileRow : files) {
				String fileId = fileRow.get(0);
				long size = Long.parseLong(fileRow.get(1));
				String fileName = fileRow.get(2);
				long ownerUserId = Long.parseLong(fileRow.get(3));
				if (ownerUserId == userId) {
					printFile(fileName, size);
				}
			}
		}
	}

	/*
	 * Important note - I have decided to load all data from these two csv files before writing them to csv report.
	 * 
	 * 
	 * */
	private void printToCsv() {
		try {
			//Create a new csv file
			PrintWriter pw = new PrintWriter(new File("report.csv"));
			StringBuilder sb = new StringBuilder();
			for (List<String> userRow : users) {
				long userId = Long.parseLong(userRow.get(0));
				String userName = userRow.get(1);
				for (List<String> fileRow : files) {
					long size = Long.parseLong(fileRow.get(1));
					String fileName = fileRow.get(2);
					long ownerUserId = Long.parseLong(fileRow.get(3));
					if (ownerUserId == userId) {
						sb.append(userName + ", " + fileName + ", " + size + "\n");
					}
				}
			}
			pw.write(sb.toString());
			pw.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			System.out.println("An exception has occured. ");
		}
		System.out.println("Print to CSV finished successfully. ");
	}

	
	//Method to load to plain text from csv files, parameters are for csv files located inside a project.
	private void loadData(String userFn, String filesFn) throws IOException {
		String line;

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(userFn));
			users = new ArrayList<List<String>>();

			reader.readLine(); // skip header

			while ((line = reader.readLine()) != null) {
				users.add(Arrays.asList(line.split(",")));
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}

		reader = null;
		try {
			reader = new BufferedReader(new FileReader(filesFn));
			files = new ArrayList<List<String>>();

			reader.readLine(); // skip header

			while ((line = reader.readLine()) != null) {
				files.add(Arrays.asList(line.split(",")));
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	//Just a method to print some 
	private void printHeader() {
		System.out.println("Audit Report");
		System.out.println("============");
	}

	private void printUserHeader(String userName) {
		System.out.println("## User: " + userName);
	}

	private void printFile(String fileName, long fileSize) {
		System.out.println("* " + fileName + " ==> " + fileSize + " bytes");
	}

	public void topFiles(long topNfiles) {
		filesAsObjects();
		getFilesObj().sort((Csv c1, Csv c2)->c1.getFileSize().compareTo(c2.getFileSize()));
		if (topNfiles > filesObj.size()){
			System.out.println("Invalid parameter - there is not " + topNfiles + " files in the csv. ");
			return;
		}
		for (int i=0; i <= topNfiles; i++){
			System.out.println(". " + filesObj.get(i).toString());
		}
	}
	
	
	//For easier compare through csv data by file size, I have decided to present csv data as an object.
	private void filesAsObjects() {
		if (filesObj == null || filesObj.isEmpty()) {
			for (List<String> userRow : users) {
				long userId = Long.parseLong(userRow.get(0));
				String userName = userRow.get(1);
				for (List<String> fileRow : files) {
					long size = Long.parseLong(fileRow.get(1));
					String fileName = fileRow.get(2);
					long ownerUserId = Long.parseLong(fileRow.get(3));
					if (ownerUserId == userId) {
						getFilesObj().add(new Csv(fileName, size, ownerUserId));

					}
				}
			}
		}
	}

}
