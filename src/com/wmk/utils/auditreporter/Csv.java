package com.wmk.utils.auditreporter;

public class Csv {

	
	private String fileName;
	
	private Long fileSize;
	
	private long ownerId;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}

	public Csv(String fileName, long fileSize, long ownerId) {
		super();
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.ownerId = ownerId;
	}

	@Override
	public String toString() {
		return "" + fileName + ", " + fileSize + ", " + ownerId ;
	}
	
	
}
